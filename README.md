# GitLab PureScript Page

A GitLab Page using PureScript

## Getting started

Fork me

The PureScript homepage can be found [here](https://www.purescript.org/).

You can find help with PureScript at the [Getting Started link](https://github.com/purescript/documentation/blob/master/guides/Getting-Started.md).

The Awesome List for PureScript can be found [here](https://github.com/passy/awesome-purescript#readme).

You can edit the repository locally by using `git clone` using the following commands:
```
git clone https://gitlab.com/Ninja5tyl3/gitlab-purescript-page.git
cd gitlab-purescript-page
```
And then you will be able to edit and build as much as you want.

If you want to use an existing directory, use the following commands:
```
cd existing_repo
git remote add origin https://gitlab.com/Ninja5tyl3/gitlab-purescript-page.git
git branch -M main
git push -uf origin main
```

Be sure to change the origin to your own repository.
